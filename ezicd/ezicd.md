@page ezicd

# ezicd

This is a placeholder for the homepage of your documentation.

## Testing

Open [cookbook/test.html](../test.html)

## Building

Run:

    > ./js ezicd/scripts/build.js
    
## Documentation

Run:

    > ./js ezicd/scripts/docs.js