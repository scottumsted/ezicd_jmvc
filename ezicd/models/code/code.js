steal('can', function (can) {
	/**
	 * @constructor ezicd/models/code
	 * @alias Code
	 * @parent ezicd
	 * @inherits can.Model
	 *
	 * Wraps backend code services.
	 */
	return can.Model(
	/* @static */
	{
		defaults: {
            searchTerm: "",
            icd10Code: "",
            icd10Desc: ""
        },
        /**
 		 * Find all codes
		 */
		find : function(params){
            var url = "http://api.wuppsy.com/icd10Lookup.json/"+params;
            return $.ajax({
                url: url,
                dataType: 'jsonp'
            });
        }
	},
	/* @Prototype */
	{});
});