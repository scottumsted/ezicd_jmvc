steal( "./code.js", 
	   "funcunit/qunit", 
	   "ezicd/models/code/code.js/models/fixtures", 
	   function( Code ){
	   	
	module("ezicd/models/code");
	
	test("findAll", function(){
		expect(4);
		stop();
		Code.findAll({}, function(codes){
			ok(codes, "findAll provides an object")
	        ok(codes.length, "findAll provides something array-like")
	        ok(codes[0].name, "findAll provides an object with a name")
	        ok(codes[0].description, "findAll provides an object with a description")
			start();
		});
	});
	
	test("create", function(){
		expect(3)
		stop();
		new Code({name: "dry cleaning", description: "take to street corner"}).save(function(code) {
			ok(code, "save provides an object");
			ok(code.id, "save provides and object with an id");
			equals(code.name,"dry cleaning", "save provides an objec with a name")
			code.destroy()
			start();
		});
	});

	test("update" , function(){
		expect(2);
		stop();
		new Code({name: "cook dinner", description: "chicken"}).save(function(code) {
			equals(code.description,"chicken", "save creates with description");
			code.attr({description: "steak"}).save(function(code){
				equals(code.description,"steak", "save udpates with description");
				code.destroy();
				start();
			});
        });
	});

	test("destroy", function(){
		expect(1);
		stop();
		new Code({name: "mow grass", description: "use riding mower"}).destroy(function(code) {
			ok( true ,"Destroy called" )
			start();
		});
	});
});