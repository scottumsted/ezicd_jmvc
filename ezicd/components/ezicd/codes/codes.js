steal('can',
    './init.ejs',
    'ezicd/models/code/code.js',
    function(can, initEJS, Code){
    /**
     * @class ezicd/ezicd/codes
	 * @alias Codes   
     */
    return can.Control(
	/** @Static */
	{
		defaults : {}
	},
	/** @Prototype */
	{
		init : function(){
			this.element.html(can.view('ezicd/components/ezicd/codes/init.ejs',{}));
		},
        "{searchObserver} change": function(currentSearch, ev, newSearch){
            var sfunc = can.proxy(this.codeSuccess, this);
            var efunc = can.proxy(this.codeError, this);
            $.when(Code.find(newSearch.terms).done(sfunc).fail(efunc));
        },
        codeSuccess : function(data){
            if (data.count > 0) {
                $("#codeCount").html(data.count.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
            } else {
                $("#codeCount").html('&nbsp;');
            }
            $("#icd10LookupResult").html('&nbsp;');
            $.each(data.codes, function (index, codeData) {
                var result = "<div class='row'><div class='col-lg-2 lead' style='cursor:pointer;'>" + codeData.code + "</div><div class='col-lg-10 lead' style='cursor:pointer;'>" + codeData.description + "</div></div>";
                $("#icd10LookupResult").append(result);
            });
        },
        codeError : function(x,text, error){
            $("#codeCount").html(x+' - '+text+' - '+error);
            $("#icd10LookupResult").html('&nbsp;');
        }
	});
});