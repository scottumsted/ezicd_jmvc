steal('ezicd/ezicd/codes','funcunit', function( Codes, S ) {

	module("ezicd/ezicd/codes", { 
		setup: function(){
			S.open( window );
			$("#qunit-test-area").html("<div id='codes'></div>")
		},
		teardown: function(){
			$("#qunit-test-area").empty();
		}
	});
	
	test("updates the element's html", function(){
		new Codes('#codes');
		ok( $('#codes').html() , "updated html" );
	});

});