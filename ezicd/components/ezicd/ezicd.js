steal('can',
    './init.ejs',
    './codes/codes',
    './omnibox/omnibox',
    'ezicd/models/code/code.js',
    function(can, initEJS, Codes, Omnibox, Code){
    /**
     * @class ezicd/ezicd
	 * @alias Ezicd   
     */
    return can.Control(
	/** @Static */
	{
		defaults : {
            codeModel: new Code()
        }
	},
	/** @Prototype */
	{
		init : function(){
            var searchObserver = new can.compute();
            this.element.html(initEJS({})).promise().done(
                can.proxy(
               function(el){
                   new Omnibox('#omnibox', {searchObserver: searchObserver});
                   new Codes('#codes', {searchObserver: searchObserver});
               },this)
            );
		}
	});
});