steal('ezicd/ezicd','funcunit', function( Ezicd, S ) {

	module("ezicd/ezicd", { 
		setup: function(){
			S.open( window );
			$("#qunit-test-area").html("<div id='ezicd'></div>")
		},
		teardown: function(){
			$("#qunit-test-area").empty();
		}
	});
	
	test("updates the element's html", function(){
		new Ezicd('#ezicd');
		ok( $('#ezicd').html() , "updated html" );
	});

});