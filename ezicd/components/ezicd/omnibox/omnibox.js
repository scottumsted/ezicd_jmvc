steal('can',
    './init.ejs',
    function(can, initEJS){
    /**
     * @class ezicd/ezicd/omnibox
	 * @alias Omnibox   
     */
    return can.Control(
	/** @Static */
	{
		defaults : {

        }
	},
	/** @Prototype */
	{
		init : function(){
            var i = initEJS;
			this.element.html(can.view('ezicd/components/ezicd/omnibox/init.ejs',{}));
		},
        '#icd10LookupSearchTerm keyup': function(el, ev){
            var terms = el.val().split(/[.,\/ -]/);
            var pTerms = new Array();
            var pIndex = 0;
            $.each(terms, function (index, termData) {
                if (termData.trim().length >= 3) {
                    pTerms[pIndex++] = termData.trim();
                }
            });
            if(pTerms.length > 0){
                this.options.searchObserver({terms:pTerms});
            }
        }
    }
    );
});