steal('ezicd/ezicd/omnibox','funcunit', function( Omnibox, S ) {

	module("ezicd/ezicd/omnibox", { 
		setup: function(){
			S.open( window );
			$("#qunit-test-area").html("<div id='omnibox'></div>")
		},
		teardown: function(){
			$("#qunit-test-area").empty();
		}
	});
	
	test("updates the element's html", function(){
		new Omnibox('#omnibox');
		ok( $('#omnibox').html() , "updated html" );
	});

});