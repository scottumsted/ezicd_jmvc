steal(
	'funcunit',
	function (S) {

	// this tests the assembly 
	module("ezicd", {
		setup : function () {
			S.open("//ezicd/index.html");
		}
	});

	test("welcome test", function () {
		equals(S("h1").text(), "Welcome to JavaScriptMVC!", "welcome text");
	});

});
