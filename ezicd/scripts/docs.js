//js ezicd/scripts/doc.js

load('steal/rhino/rhino.js');
steal("documentjs", function(DocumentJS){
	DocumentJS('ezicd/index.html', {
		out: 'ezicd/docs',
		markdown : ['ezicd', 'steal', 'jquerypp', 'can', 'funcunit'],
		parent : 'ezicd'
	});
});