steal('todos/nav','funcunit', function( Nav, S ) {

	module("todos/nav", { 
		setup: function(){
			S.open( window );
			$("#qunit-test-area").html("<div id='nav'></div>")
		},
		teardown: function(){
			$("#qunit-test-area").empty();
		}
	});
	
	test("updates the element's html", function(){
		new Nav('#nav');
		ok( $('#nav').html() , "updated html" );
	});

});