steal('can','./init.ejs', function(can, initView){
    /**
     * @class todos/nav
	 * @alias Nav   
     */
    return can.Control(
	/** @Static */
	{
		defaults : {}
	},
	/** @Prototype */
	{
		init : function(){
			this.element.html(initView({
				message: "Hello World from Nav"
			}));
		}
	});
});