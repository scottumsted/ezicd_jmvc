# EZICD JMVC#

This is a JMVC (CanJS) client to search ICD10 codes. It's hosted at http://ezicd.ezpz.gs/jmvc/ezicd/. There's a non-JMVC version as well hosted at http://ezicd.ezpz.gs.

This calls an API I've hosted at api.wuppsy.com.